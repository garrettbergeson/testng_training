package com.progleasing;

import org.testng.annotations.Factory;

public class SampleFactoryTestNGTest {

	@Factory
	public Object[] factoryTest(){
		return new Object[] {
				new SampleTestNGTest(),
				new SampleTestNGTest("Test 1"),
				new SampleTestNGTest("Test 2"),
				new SampleTestNGTest("Test 3")
		};
	}

}
