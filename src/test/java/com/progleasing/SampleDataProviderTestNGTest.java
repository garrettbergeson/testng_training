package com.progleasing;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;;


public class SampleDataProviderTestNGTest {

	@DataProvider(name="dataProviderTest")
	public Object[][] dataProviderTest(){
		return new Object[][] { { "Test 1" }, { "Test 2" }, { "Test 3" }, { "Test 4" } };
	}
	
	@Test(dataProvider = "dataProviderTest")
	public void method1(String param){
		String string1 = "test";
		String string2 = "test";
		Assert.assertEquals(string1, string2);
	}

	@BeforeMethod
	public void method2(){
		System.out.println("Before method. Method 2");
	}

	@AfterMethod
	public void method3(){
		System.out.println("After method. Method 3");
	}
	
	@Test(expectedExceptions = AssertionError.class, dataProvider = "dataProviderTest")
	public void method5(String param){ //requires exception to be thrown
		String string1 = "test";
		String string2 = "nottest";
		Assert.assertEquals(string1, string2);
	}

	@BeforeClass
	public void method7(){
		System.out.println("Before class. Method 7");
	}

	@AfterClass
	public void method8(){
		System.out.println("After class. Method 8");
	}

	@BeforeSuite
	public void method9(){
		System.out.println("Before suite. Method 9");
	}

	@AfterSuite
	public void method10(){
		System.out.println("After suite. Method 10");
	}
}
