package com.progleasing;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import org.testng.Assert;
import org.testng.annotations.*;

public class SampleTestNGTest
{

	public String testName;

	public SampleTestNGTest(){
		testName = "Simple Test Without Params";
	}

	public SampleTestNGTest(String testName){
		this.testName = testName;
	}

	@Test
	public void method1(){
		String string1 = "test";
		String string2 = "test";
		Assert.assertEquals(string1, string2);
	}

	@BeforeMethod
	public void method2(){
		System.out.println("Before method. Method 2");
	}

	@AfterMethod
	public void method3(){
		System.out.println("After method. Method 3");
	}

	@Test(enabled=false)
	public void method4(){ //method fails
		String string1 = "test";
		String string2 = "nottest";
		Assert.assertEquals(string1, string2);
	}

	@Test(expectedExceptions = AssertionError.class)
	public void method5(){ //requires exception to be thrown
		String string1 = "test";
		String string2 = "nottest";
		Assert.assertEquals(string1, string2);
	}

	@Test(expectedExceptions = AssertionError.class, enabled=false)
	public void method6(){
		String string1 = "test";
		String string2 = "test";
		Assert.assertEquals(string1, string2);
	}

	@BeforeClass
	public void method7(){
		System.out.println("Before class. Method 7");
	}

	@AfterClass
	public void method8(){
		System.out.println("After class. Method 8");
	}

	@BeforeSuite
	public void method9(){
		System.out.println("Before suite. Method 9");
	}

	@AfterSuite
	public void method10(){
		System.out.println("After suite. Method 10");
	}

}

